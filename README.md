## About Media entity Unsplash

This module provides Unsplash integration for Media (i.e. media type provider
plugin).

### Unsplash API
NOTICE: A NORMAL API ACCESS ALLOWS 50 CALLS PER HOUR. THERE IS ONE CALL DONE FOR EACH SEARCH, 
        ONE CALL DONE FOR EACH ADD, AND ONE CALL FOR EACH CLICK TO THE NEXT/PREV PAGE. PLEASE 
        KEEP THIS IN MIND!
        
To gain Unsplash API credentials, visit https://unsplash.com/login and click the "Join" button
below login.

To install the module you must have the core version of media installed as well as the Crop API module!

After enabling the module, you must visit /admin/config/media/media_entity_unsplash and enter your
API credentials. At this time, only the Access Key is used. Other fields are available for future
use.

The media type with fields is added automatically via the install config.

There are two options to add an Unsplash image to your media library:

1. Visit /media/add/unsplash and add an ID for an unsplash image to the "Unsplash ID" field, as
   well as a name. You can find the ID in the image path for the unsplash image. In the URL 
   https://unsplash.com/photos/HJCywuQqKYY the ID is HJCywuQqKYY.
    
2. The second (and better) option is to go to /admin/config/media/media_entity_unsplash/search
   Type a key word in the search box and click search. Once the images load you will have an add
   button below each image. Click that button and the image will be added as a media item that can
   be seen at /admin/content/media.

Project page: http://drupal.org/project/media_entity_unsplash

Maintainers:
 - Steven (Jake) Wilmes (@swilmes) https://www.drupal.org/u/swilmes

IRC channel: #drupal-media
