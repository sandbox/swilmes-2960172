<?php

namespace Drupal\media_entity_unsplash\Plugin\media\Source;

use Drupal\media_entity_unsplash\UnsplashEmbedFetcher;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for Unsplash.
 *
 * @MediaSource(
 *   id = "unsplash",
 *   label = @Translation("Unsplash"),
 *   description = @Translation("Provides business logic and metadata for Unsplash."),
 *   allowed_field_types = {"string", "string_long", "link"},
 *   default_thumbnail_filename = "unsplash.png"
 * )
 */
class Unsplash extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * The unsplash fetcher.
   *
   * @var \Drupal\media_entity_unsplash\UnsplashEmbedFetcher
   */
  protected $fetcher;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\media_entity_unsplash\UnsplashEmbedFetcher $fetcher
   *   Unsplash fetcher service.
   * @param \GuzzleHttp\Client $httpClient
   *   Guzzle client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, FieldTypePluginManagerInterface $field_type_manager, UnsplashEmbedFetcher $fetcher, Client $httpClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->fetcher = $fetcher;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('media_entity_unsplash.unsplash_embed_fetcher'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'shortcode' => $this->t('Unsplash ID'),
      'id' => $this->t('Media ID'),
      'type' => $this->t('Media type: image or video'),
      'image_uri' => $this->t('Link to the full image'),
      'thumbnail' => $this->t('Link to the thumbnail'),
      'thumbnail_local' => $this->t("Copies thumbnail locally and return it's URI"),
      'thumbnail_local_uri' => $this->t('Returns local URI of the thumbnail'),
      'username' => $this->t('Author of the post'),
      'caption' => $this->t('Caption'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $id = $media->get('field_media_unsplash')->value;
    $image = $this->fetcher->fetchUnsplashEmbed($id);
    if ($attribute_name == 'default_name') {
      // Try to get some fields that need the API, if not available, just use
      // the shortcode as default name.
      $username = $image['user']['name'];
      return $username . ' - ' . $id;
    }

    if ($attribute_name == 'thumbnail_uri') {
      $uri = $this->configFactory->get('media_entity_unsplash.settings')->get('local_images') . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';
      return $uri;
    }

    switch ($attribute_name) {
      case 'id':
        return  $id;

      case 'type':
        return 'image';

      case 'image_uri':
        $uri = $this->configFactory->get('media_entity_unsplash.settings')->get('local_images') . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';
        return $image['urls']['full'];

      case 'thumbnail':
        $uri = $this->configFactory->get('media_entity_unsplash.settings')->get('local_images') . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';
        return $image['urls']['thumb'];

      case 'thumbnail_local':
        if (isset($image['urls']['full'])) {
          $local_uri = $this->configFactory->get('media_entity_unsplash.settings')->get('local_images') . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION);
          $local_uri .= 'png';
        }
        $directory = dirname($local_uri);
        file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

        $response = $this->httpClient->get($image['urls']['full']);
        if ($response->getStatusCode() == 200) {
          return file_unmanaged_save_data($response->getBody(), $local_uri, FILE_EXISTS_REPLACE);
        }
        return FALSE;

      case 'thumbnail_local_uri':
        if (isset($image['urls']['full'])) {
          $uri = $this->configFactory->get('media_entity_unsplash.settings')->get('local_images') . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';
          return $uri;
        }
        return FALSE;

      case 'username':
        return $image['user']['username'];

      case 'caption':
        return $image['user']['name'];
    }


    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return ['UnsplashEmbedCode' => []];
  }
}
