<?php

namespace Drupal\media_entity_unsplash\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views\Views;

/**
 * Provides a block for the searching of Unsplash for images.
 *
 * @Block(
 *   id = "media_entity_unsplash_search",
 *   admin_label = @Translation("Unsplash Search Block")
 * )
 */
class UnsplashSearch extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $path = '/' . drupal_get_path('module', 'media_entity_unsplash');

    return [
      '#theme' => 'media_entity_unsplash_search',
      '#rows' => null,
      '#module_path' => $path,
      '#cache' => [
        'max-age' => 0
      ]
    ];
  }

}
