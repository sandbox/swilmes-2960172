<?php

namespace Drupal\media_entity_unsplash\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Symfony\Component\HttpFoundation\Response;
use Drupal\media_entity_unsplash\UnsplashEmbedFetcher;
use Drupal\media\Entity\Media;

/**
 * Configures Unsplash API settings for this site.
 */

class SearchFormController extends ControllerBase {
  /**
   * {@inheritdoc}
   */
  public function process() {
    $build['unsplash_search'] = \Drupal::service('plugin.manager.block')
      ->createInstance('media_entity_unsplash_search')
      ->build();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveImages() {
    $query = $_GET['query'];
    $page = isset($_GET['pager']) ? $_GET['pager'] : '1';
    if ($query) {
      $rows = [];
      $access_key = \Drupal::config('media_entity_unsplash.settings')->get('access_key');
      $url = 'https://api.unsplash.com/search/photos?query=' . $query . '&page='. $page .'&per_page=30';

      try {
        $response = \Drupal::httpClient()->get($url, array('headers' => array('Authorization' => 'Client-ID ' . $access_key)));
        $data = json_decode($response->getBody(), TRUE);

        if (empty($data)) {
          $out = '<div class="empty">Empty Response</div>';
          $build = [
            '#markup' => $out,
          ];
        }
        else {
          $data['query'] = $query;
          $data['prev'] = NULL;
          $data['next'] = NULL;
          $i = 0;
          $crop_string = '?ixlib=rb-0.3.5&q=80&fit=crop&h=400&w=400';
          foreach ($data['results'] as $result) {
            $data['results'][$i]['urls']['sized'] = preg_replace('/\?.*/', '', $result['urls']['full']) . $crop_string;
            $i++;
          }
          if ($page > 1) {
            $data['prev'] = $page - 1;
          }

          if ($page < $data['total_pages']){
            $data['next'] = $page + 1;
          }

          $build = array(
            '#theme' => 'media_entity_unsplash_results',
            '#rows' => $data,
            '#cache' => [
              'max-age' => 0
            ]
          );
        }
      } catch (RequestException $e) {
        $out = '<div class="empty">Exception Thrown</div>';
        $build = [
          '#markup' => $out,
        ];
      }
    }
    else {
      $out = '<div class="empty">No Query Made</div>';
      $build = [
        '#markup' => $out,
      ];
    }
    // This is the important part, because will render only the TWIG template.
    return new Response(render($build));
  }

  /**
   * {@inheritdoc}
   */
  public function addMedia() {
    $id = $_GET['id'];
    \Drupal::logger('media_entity_unsplash')->notice($id);

    $values = [
      'targetEntityType' => "media",
      'bundle' => "unsplash",
      'field_media_unsplash' => $id,
      'status' => true
    ];

    $imageEntity = Media::create($values);
    $imageEntity->save();

    $response_array = ['status' => 'success', 'data' => ''];
    $response = new Response(json_encode($response_array));
    $response->headers->set('Content-Type', 'application/json; charset=utf-8');
    return $response;
  }
}
