<?php

namespace Drupal\media_entity_unsplash;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Fetches unsplash image data.
 *
 * Fetches (and caches) Unsplash post data based on ID.
 */
class UnsplashEmbedFetcher implements UnsplashEmbedFetcherInterface {

  const UNSPLASH_URL = 'https://images.unsplash.com/';

  const UNSPLASH_API = 'https://api.unsplash.com/';

  /**
   * The optional cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * InstagramEmbedFetcher constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   A HTTP Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   A logger factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface|null $cache
   *   (optional) A cache bin for storing fetched unsplash photos.
   */
  public function __construct(Client $client, LoggerChannelFactoryInterface $loggerFactory, CacheBackendInterface $cache = NULL) {
    $this->httpClient = $client;
    $this->loggerFactory = $loggerFactory;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchUnsplashEmbed($shortcode) {

    $access_key = \Drupal::config('media_entity_unsplash.settings')->get('access_key');
    $url = 'https://api.unsplash.com/photos/' . $shortcode;

    try {
      $response = \Drupal::httpClient()->get($url, array('headers' => array('Authorization' => 'Client-ID ' . $access_key)));
      $data = json_decode($response->getBody(), TRUE);

      if (!empty($data)) {
        return $data;
      }
    } catch (RequestException $e) {
      // Logs an error
      \Drupal::logger('media_entity_unsplash')->error('Failed to load image from id. Exception: ' . $e);
    }

    return FALSE;
  }

}
